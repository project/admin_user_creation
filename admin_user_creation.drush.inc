<?php
/**
 * @file
 * Admin User Creation drush command file.
 *
 * This file contain different functions that using in CLI.
 */

/**
 * Implements hook_drush_command().
 */
function admin_user_creation_drush_command() {
  $commands['create-user'] = array(
    'description' => 'Create User With Admin Role',
    'callback'    => 'custom_user_creation',
    'aliases' => array('create-user'),
    'arguments' => array(
      'arg1' => 'user name',
      'arg2' => 'user email',
      'arg3' => 'user pass',
    ),
    'examples' => array(
      'drush cu error' => 'Prints error as argument is blank.',
    ),
  );
  return $commands;
}

/**
 * Callback function for hook_drush_command().
 */
function custom_user_creation($arg1 = '', $arg2 = '', $arg3 = '') {
  $no_of_user = variable_get('no_of_user');
  $count = 1;
  $limit = variable_get('lim');
  if ($limit) {
    $default_number = $limit;
  }
  else {
    $default_number = $count;
    variable_set('lim', $count);
  }
  if ($no_of_user >= $default_number) {
    if ($arg1 == '' || $arg2 == '' || $arg3 == '') {
      drupal_set_message(t('All the auguments are missing.need to give 3 auguments like this username usermail userpass'));
    }
    else {
      $user_exist_by_name = user_load_by_name($arg1);
      $user_exist_by_mail = user_load_by_name($arg2);
      if (!$user_exist_by_name && !$user_exist_by_mail) {
        $new_user = array(
          'name' => $arg1,
          'pass' => $arg3,
          'mail' => $arg2,
          'status' => 1,
          'init' => $arg2,
          'roles' => array(
            DRUPAL_AUTHENTICATED_RID => 'authenticated user',
            3 => 'administrator',
          ),
        );
        // note: do not md5 the password.
        user_save('', $new_user);
        $default_number = $default_number + 1;
        variable_set('lim', $default_number);
        drupal_set_message(t('User has been created successfully'));
      }
      else {
        drupal_set_message(t('Username and Email Id should be unique'));
      }
    }
  }
  else {
    drupal_set_message(t('Sorry You have exceed the limit to create user from drush'));
  }
}
