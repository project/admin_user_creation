
-- SUMMARY --

This admin_user_creation module creates users having administration role with
a single line drush command. The command line looks like the one give below
  " drush create-user username user@email.com password "
It should have username , user email id and password in the sequence given above 
failing which it gives alert message "All the auguments are missing.need to give 3 
auguments like this username usermail userpass".
Make sure the username and user email provided should be unique in the project 
failing which it gives alert message "Username and Email Id should be unique".
 


For a full description of the module, visit the project page:
  https://www.drupal.org/project/admin_user_creation

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/admin_user_creation


-- REQUIREMENTS --

Nothing is required for now.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* In order to set limit to number of users with admin access that can be added to any 
project using this module, we need to do below configuration. The site admin can spacify 
maximum limit using config path given below

  Configuration-  admin >> config >> system >> user-settings



-- CONTACT --

Current maintainers:

* Bhaskar Chakraborty - https://www.drupal.org/u/bhaskar-chakraborty
* Aditya Singh - https://www.drupal.org/u/Aditya_Singh

